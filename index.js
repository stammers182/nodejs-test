process.env.TZ = 'Europe/Amsterdam';
console.log(new Date());
var express = require('express')
  , stylus = require('stylus')
  , nib = require('nib')
  , mysql = require('mysql')
  , http = require('http')
  , morgan = require('morgan');

var app = express();
function compile(str,path) {
    return stylus(str)
            .set('filename',path)
            .use(nib());
}

app.set('views',__dirname + '/views')
app.set('view engine', 'jade')
app.use(morgan('dev'));
app.get('/', function (req, res) {
  var  hour = new Date().getHours()
     , title = ''
     , animal = ''
     , count = ''
     , image = '';
     
  if(hour < 12) {
      title = 'You found a Elephant';
      animal = 'Elephant';
      count = 0;
      image = 'http://178.62.116.220/images/elephant.jpg';
  } else {
      title = 'You found a Capybara';
      animal = 'Capybara';
      count = 0;
      image = 'http://178.62.116.220/images/capybara.jpg';
  }
  res.render('index',
  { 
      title : title ,
      animal : animal,
      count : count ,
      image : image ,
  }
  );
})
app.listen(3000);